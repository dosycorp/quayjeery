export const Q = {
  buildIn,
  ael, rel
};

export function ael(types, funcs, options, el) {
  return mel.call(this, types, funcs, options, el, 'addEventListener');
}

export function rel(types, funcs, options, el) {
  return mel.call(this, types, funcs, options, el, 'removeEventListener');
}

function mel(types, funcs, options, target, funcName) {
  let Attach = EventTarget.prototype[funcName];
  target = target || this;

  if ( !(target instanceof EventTarget) ) {
    console.log(target, this);
    throw new TypeError(`ael requires fourth argument, or this, be type EventTarget`);
  }

  if ( Attach == ael ) {    // then ael is on EventTarget prototype, so
    // find the original by buildIn()'s convention
    Attach = EventTarget.prototype[`$${funcName}`];
  }

  funcName = `$${funcName}`;

  if ( !Array.isArray(types) ) {
    types = [types];
  } 

  if ( !Array.isArray(funcs) ) {
    funcs = [funcs];
  }

  for( const func of funcs ) {
    for( const type of types ) {
      EventTarget.prototype[funcName].call(target, type, func, options);
    }
  }
}

function onAll(types, funcs, opts) {
  this.forEach(target => target instanceof EventTarget && target.on(types, funcs, opts));
}

function offAll(types, funcs, opts) {
  this.forEach(target => target instanceof EventTarget && target.off(types, funcs, opts));
}

function filterSelector(selector) {
  return this.filter(node => node.matches(selector));
}

function buildIn() {
  HTMLAllCollection.prototype.addEventListener = HTMLAllCollection.prototype.on = onAll;
  HTMLAllCollection.prototype.removeEventListener = HTMLAllCollection.prototype.off = offAll;
  HTMLAllCollection.prototype.__proto__ = Array.prototype;

  HTMLCollection.prototype.addEventListener = HTMLCollection.prototype.on = onAll;
  HTMLCollection.prototype.removeEventListener = HTMLCollection.prototype.off = offAll;
  HTMLCollection.prototype.__proto__ = Array.prototype;

  NodeList.prototype.addEventListener = NodeList.prototype.on = onAll;
  NodeList.prototype.removeEventListener = NodeList.prototype.off = offAll;
  NodeList.prototype.__proto__ = Array.prototype;

  EventTarget.prototype.$addEventListener = EventTarget.prototype.addEventListener;
  EventTarget.prototype.addEventListener = EventTarget.prototype.on = ael;

  EventTarget.prototype.$removeEventListener = EventTarget.prototype.removeEventListener;
  EventTarget.prototype.removeEventListener = EventTarget.prototype.off = rel;

  NodeList.prototype.$ = NodeList.prototype.filterSelector = filterSelector;
  Node.prototype.$ = Node.prototype.querySelector;
  Node.prototype.$$ = Node.prototype.querySelectorAll;

  self.on = self.on.bind(self);
  self.off = self.off.bind(self);

  self.$ = document.querySelector.bind(document);
  self.$$ = document.querySelectorAll.bind(document);

}
