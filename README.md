# QuayJeery

Like a better version of JQuery without all the poppycock. Inspired by [bling](http://git.io/blingjs).

Also inspired by [resig](https://github.com/jeresig/nodelist).

## This is only the beginning

Let's make this a useful utility belt.

Possible future directions:

- [cash](https://github.com/kenwheeler/cash)

But I'm thinking more like, minimal DOM manipulation, and a lot of other useful functions for manipulating data, a la [lodash](https://lodash.com/docs/4.17.15) but more with a focus on objects, JSON, unicode, i18n, dates. 

You know, all those modern conveniences. But crammed into one tiny high-perf package.

Something like that.


